# Ruby on Rails Sample Application

This is the sample app from the famous [*Ruby on Rails Tutorial: Learn Web Development with Rails*](http://www.railstutorial.org/) by [Michael Hartl](http://www.michaelhartl.com/).

## Getting started

To get started with this microblogging application, clone this repository and install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the tests pass, run the app in a local server:

```
$ rails server
```

For more info, see the [*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).
