FROM ruby:2.3
MAINTAINER kfrz.code@gmail.com

# Install Rails dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    nodejs

RUN mkdir -p /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN gem install bundler && bundle install --jobs 20 --retry 5

COPY . ./

CMD bundle exec rails s -b 0.0.0.0